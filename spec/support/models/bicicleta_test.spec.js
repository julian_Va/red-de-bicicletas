var Bicicleta = require('../../../models/Bicicleta');


beforeEach(() => {Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregar una bicicleta', () => {

        expect(Bicicleta.allBicis.length).toBe(0);

        let c = new Bicicleta (3,"violeta","tundra", [3.4059855,-76.5319854]);
        Bicicleta.add(c);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(c);
    });
});

describe('Bicicleta.findByid', () => {
    it('debe devolver la bicicleta con id:3', () => {

        expect(Bicicleta.allBicis.length).toBe(0);

        let c = new Bicicleta (3,"violeta","tundra", [3.4059855,-76.5319854]);
        let d = new Bicicleta (4,"amarillo","pista", [3.8059855,-76.9319854]);
        Bicicleta.add(c);
        Bicicleta.add(d);

        let targetBici = Bicicleta.findByid(3);
        expect(targetBici.id).toBe(3);
        expect(targetBici.color).toBe(c.color);
        expect(targetBici.modelo).toBe(c.modelo);

    });
});

describe('Bicicleta.remove', () => {
    it('debe borrar la biciclete con id:3', () => {

        expect(Bicicleta.allBicis.length).toBe(0);

        let c = new Bicicleta (3,"violeta","tundra", [3.4059855,-76.5319854]);        
        Bicicleta.add(c);
        
        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.removeById(3);
        expect(Bicicleta.allBicis.length).toBe(0);

    });
});